﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using RepairShop.Models;

namespace RepairShop.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly AppDbContext _context;

    public HomeController(ILogger<HomeController> logger, AppDbContext context)
    {
        _logger = logger;
        _context = context;
    }

    public IActionResult Index()
    {
        var totalTickets = _context.Ticket.Count();
        var activeTickets = _context.Ticket.Where(t => t.Completed == false).Count();
        var completedTickets = _context.Ticket.Where(t => t.Completed == true).Count();

        List<int> count = new List<int>();
        count.Add(totalTickets);
        count.Add(activeTickets);
        count.Add(completedTickets);
        return View(count);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
